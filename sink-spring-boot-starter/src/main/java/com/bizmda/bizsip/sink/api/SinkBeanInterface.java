package com.bizmda.bizsip.sink.api;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;

/**
 * sink-bean类型的Sink服务类接口
 * @author 史正烨
 */
public interface SinkBeanInterface {
    /**
     * JSONObject sink-bean的服务处理实现方法
     * @param jsonObject 传入的消息
     * @return 返回值
     * @throws BizException Biz-SIP异常
     */
    JSONObject process(JSONObject jsonObject) throws BizException;
}
