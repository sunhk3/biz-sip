package com.bizmda.bizsip.sink.cmdexe;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;

/**
 * @author shizhengye
 */
public abstract class AbstractSinkBeanCmdExe {
    /**
     * JSONObject SinkBean服务调用
     * @param inJsonObject 传入的消息
     * @return 返回值
     */
    public abstract JSONObject execute(JSONObject inJsonObject) throws BizException;
}
