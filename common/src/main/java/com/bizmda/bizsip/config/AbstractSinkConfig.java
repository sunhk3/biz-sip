package com.bizmda.bizsip.config;

import cn.hutool.core.text.CharSequenceUtil;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 史正烨
 */
@Data
public class AbstractSinkConfig {
    public static final int TYPE_REST = 0;
    public static final int TYPE_RABBITMQ = 1;
    protected static final String[] MESSAGE_TYPE = {AbstractSinkConfig.SINK_TYPE_REST,AbstractSinkConfig.SINK_TYPE_RABBITMQ};
    public static final int PROCESSOR_DEFAULT = 0;
    public static final int PROCESSOR_SINK_BEAN = 1;
    public static final int PROCESSOR_BEAN = 2;
    protected static final String[] MESSAGE_PROCESSOR = {AbstractSinkConfig.SINK_PROCESSOR_DEFAULT,AbstractSinkConfig.SINK_PROCESSOR_SINK_BEAN,AbstractSinkConfig.SINK_PROCESSOR_BEAN};
    private static final String SINK_TYPE_REST = "rest";
    private static final String SINK_TYPE_RABBITMQ = "rabbitmq";
    private static final String SINK_PROCESSOR_DEFAULT = "default";
    private static final String SINK_PROCESSOR_SINK_BEAN = "sink-bean";
    private static final String SINK_PROCESSOR_BEAN = "bean";
    private String id;
    private int type;
    private int processor;
    private String className;

    private Map<String,Object> converterMap;
    private Map<String,Object> connectorMap;

    private Map<String,Object> circuitBreakerMap;

    public AbstractSinkConfig(Map<String,Object> map) {
        this.id = (String)map.get("id");
        String myType = (String)map.get("type");
        this.type = TYPE_REST;
        if (SINK_TYPE_RABBITMQ.equalsIgnoreCase(myType)) {
            this.type = TYPE_RABBITMQ;
        }
        String myProcessor = (String)map.get("processor");
        this.processor = PROCESSOR_DEFAULT;
        if (SINK_PROCESSOR_SINK_BEAN.equalsIgnoreCase(myProcessor)) {
            this.processor = PROCESSOR_SINK_BEAN;
        }
        else if (SINK_PROCESSOR_BEAN.equalsIgnoreCase(myProcessor)) {
            this.processor = PROCESSOR_BEAN;
        }
        this.className = (String) map.get("class-name");
        this.converterMap = (Map<String, Object>)map.get("converter");
        this.connectorMap = (Map<String, Object>)map.get("connector");
        this.circuitBreakerMap = (Map<String, Object>) map.get("circuit-breaker");
        if (this.circuitBreakerMap == null) {
            this.circuitBreakerMap = new HashMap<>();
        }
    }

    @Override
    public String toString() {
        return CharSequenceUtil.format("type={},processor={}",
                MESSAGE_TYPE[this.type],MESSAGE_PROCESSOR[this.processor]);
    }
}
