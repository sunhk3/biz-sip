package com.bizmda.bizsip.config;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizTools;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 史正烨
 */
@Data
public class PredicateRuleConfig {
    private String predicate;
    private String rule;

    public PredicateRuleConfig(Map<String,Object> ruleMap) {
        this.predicate = (String)ruleMap.get("predicate");
        this.rule = (String)ruleMap.get("rule");
    }

    public String getMatchRule(JSONObject data) throws BizException {
        Map<String,Object> map = new HashMap<>();
        map.put("data",data);
        if (this.predicate == null || this.predicate.isEmpty()) {
            return BizTools.getElStringResult(this.rule,map);
        }
        boolean predicateFlag = BizTools.getElBooleanResult(this.predicate,map);
        if (predicateFlag) {
            return BizTools.getElStringResult(this.rule,map);
        }
        return null;
    }
}
