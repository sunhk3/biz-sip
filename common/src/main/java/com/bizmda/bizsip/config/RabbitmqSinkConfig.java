package com.bizmda.bizsip.config;

import cn.hutool.core.text.CharSequenceUtil;
import lombok.Getter;

import java.util.Map;

/**
 * @author 史正烨
 */
@Getter
public class RabbitmqSinkConfig extends AbstractSinkConfig {
    private String routingKey;
    private String exchange;
    private String queue;

    public RabbitmqSinkConfig(Map map) {
        super(map);
        this.routingKey = (String) map.get("routing-key");
        if (CharSequenceUtil.isEmpty(this.routingKey)) {
            this.routingKey = this.getId();
        }
        this.exchange = (String) map.get("exchange");
        if (CharSequenceUtil.isEmpty(this.exchange)) {
            this.exchange = "exchange.direct.bizsip.sink";
        }
        this.queue = (String) map.get("queue");
        if (CharSequenceUtil.isEmpty(this.queue)) {
            this.queue = "queue.bizsip.sink."+this.getId();
        }
    }

    @Override
    public String toString() {
        return super.toString()
                + CharSequenceUtil.format(",exchange={},route-key={},queue={}",
        this.exchange,this.routingKey,this.queue);
    }
}
