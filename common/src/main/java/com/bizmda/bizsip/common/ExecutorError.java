package com.bizmda.bizsip.common;

import lombok.Data;

/**
 * @author shizhengye
 */
@Data
public class ExecutorError {
    private boolean isTimeoutException = false;
    private String message;
}
