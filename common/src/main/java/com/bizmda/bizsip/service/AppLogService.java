package com.bizmda.bizsip.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizConstant;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shizhengye
 */
@Slf4j
public class AppLogService {
    public static final int LOG_TYPE_APP_SUCCESS = 0;
    public static final int LOG_TYPE_APP_FAIL = 1;
    public static final int LOG_TYPE_APP_SUSPEND = 2;
    public static final int LOG_TYPE_SINK_SUCCESS = 3;
    public static final int LOG_TYPE_SINK_FAIL = 4;
    private static final String RABBITMQ_LOG_SUCCESS = "success";
    private static final String RABBITMQ_LOG_SUSPEND = "suspend";
    private static final String RABBITMQ_LOG_FAIL = "fail";
    private final RabbitTemplate rabbitTemplate;
    private final boolean isSuccessLog;
    private final boolean isSuspendLog;
    private final boolean isFailLog;

    public AppLogService(RabbitTemplate rabbitTemplate,String rabbitmqLog) {
        this.rabbitTemplate = rabbitTemplate;
        if (RABBITMQ_LOG_SUCCESS.equalsIgnoreCase(rabbitmqLog)) {
            this.isSuccessLog = true;
            this.isSuspendLog = true;
            this.isFailLog = true;
        }
        else if (RABBITMQ_LOG_SUSPEND.equalsIgnoreCase(rabbitmqLog)) {
            this.isSuccessLog = false;
            this.isSuspendLog = true;
            this.isFailLog = true;
        }
        else if (RABBITMQ_LOG_FAIL.equalsIgnoreCase(rabbitmqLog)) {
            this.isSuccessLog = false;
            this.isSuspendLog = false;
            this.isFailLog = true;
        }
        else {
            this.isSuccessLog = false;
            this.isSuspendLog = false;
            this.isFailLog = false;
        }
    }

    public void sendAppSuccessLog(BizMessage<JSONObject> inBizMessage, BizMessage<JSONObject> outBizMessage) {
        if (!this.isSuccessLog) {
            return;
        }
        sendLog(LOG_TYPE_APP_SUCCESS,inBizMessage, outBizMessage);
    }

    public void sendAppSuspendLog(BizMessage<JSONObject> inBizMessage, BizMessage<JSONObject> outBizMessage) {
        if (!this.isSuspendLog) {
            return;
        }
        sendLog(LOG_TYPE_APP_SUSPEND,inBizMessage, outBizMessage);
    }

    public void sendAppFailLog(BizMessage<JSONObject> inBizMessage, BizMessage<JSONObject> outBizMessage) {
        if (!this.isFailLog) {
            return;
        }
        sendLog(LOG_TYPE_APP_FAIL,inBizMessage, outBizMessage);
    }

    public void sendSinkSuccessLog(BizMessage<JSONObject> inBizMessage, BizMessage<JSONObject> outBizMessage) {
        if (!this.isSuccessLog) {
            return;
        }
        sendLog(LOG_TYPE_SINK_SUCCESS,inBizMessage, outBizMessage);
    }

    public void sendSinkFailLog(BizMessage<JSONObject> inBizMessage, BizMessage<JSONObject> outBizMessage) {
        if (!this.isFailLog) {
            return;
        }
        sendLog(LOG_TYPE_SINK_FAIL,inBizMessage, outBizMessage);
    }

    private void sendLog(int type,BizMessage<JSONObject> inBizMessage, BizMessage<JSONObject> outBizMessage) {
        Map<String,Object> map = new HashMap<>(3);
        map.put("type", type);
        map.put("request",inBizMessage);
        map.put("response",outBizMessage);
        log.trace("发送交易日志,类型:{}\n请求报文:\n{}\n响应报文:\n{}",type,
                BizUtils.buildBizMessageLog(inBizMessage),
                BizUtils.buildBizMessageLog(outBizMessage));
        rabbitTemplate.convertAndSend(BizConstant.BIZSIP_LOG_EXCHANGE, BizConstant.BIZSIP_LOG_ROUTING_KEY, map,
                message -> {
                    //设置消息持久化
                    message.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
                    return message;
                });
    }

}
