package com.bizmda.bizsip.sample.app.service;

import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizCircuitBreaker;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sample.sink.api.HelloInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author shizhengye
 */
@Slf4j
@Service
public class HelloAppService implements HelloInterface {
    private final HelloInterface helloInterface =  AppClientFactory.getSinkClient(HelloInterface.class,"hello-bean-sink");

    @Override
    public String hello(String message) {
        return this.helloInterface.hello(message);
    }

    @Override
    public BigDecimal addBigDecimal(BigDecimal bigDecimal) {
        return bigDecimal.add(new BigDecimal("1.0001"));
    }

    @Override
    public void doCircuitBreakerService(int a) throws BizException {
        this.helloInterface.doCircuitBreakerService(a);
    }

}
