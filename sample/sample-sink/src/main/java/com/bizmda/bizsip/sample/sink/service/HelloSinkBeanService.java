package com.bizmda.bizsip.sample.sink.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.sink.api.AbstractSinkService;
import com.bizmda.bizsip.sink.api.SinkBeanInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 史正烨
 */
@Slf4j
@Service
public class HelloSinkBeanService extends AbstractSinkService implements SinkBeanInterface {
    @Override
    public JSONObject process(JSONObject inMessage) throws BizException {
        log.debug("收到报文:\n{}", BizUtils.buildJsonLog(inMessage));
        inMessage.set("message","hello!");
        return inMessage;
    }
}
