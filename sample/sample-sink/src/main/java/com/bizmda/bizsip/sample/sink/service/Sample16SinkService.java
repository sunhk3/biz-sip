package com.bizmda.bizsip.sample.sink.service;

import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sample.sink.api.AccountDTO;
import com.bizmda.bizsip.sample.sink.api.CustomerDTO;
import com.bizmda.bizsip.sample.sink.api.Sample16Interface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shizhengye
 */
@Slf4j
@Service
public class Sample16SinkService implements Sample16Interface {
    @Override
    public String doService1(String arg1) {
        return "doService1() result";
    }

    @Override
    public void doService2(String arg1, int arg2) {
        // 无操作
    }

    @Override
    public String doService1Exception(String arg1) throws BizException {
        throw new BizException(888,"这是888错误测试!");
    }

    @Override
    public CustomerDTO queryCustomerDTO(String customerId) {
        return CustomerDTO.builder()
                .customerId("001").name("张三").age(20).sex('1').build();
    }

    @Override
    public AccountDTO[] queryAccounts(AccountDTO accountDTO) {
        AccountDTO[] accountDtos = new AccountDTO[2];
        accountDtos[0] = AccountDTO.builder().account("0001").balance(1200L).build();
        accountDtos[1] = AccountDTO.builder().account("0003").balance(45000L).build();
        return accountDtos;
    }

    @Override
    public List<AccountDTO> queryAccountList(AccountDTO accountDTO) {
        List<AccountDTO> accountDTOList = new ArrayList<>();
        accountDTOList.add(AccountDTO.builder().account("0002").balance(3400L).build());
        accountDTOList.add(AccountDTO.builder().account("0004").balance(77800L).build());
        return accountDTOList;
    }

    @Override
    public String notify(int maxRetryNum, String result) {
        return null;
    }

    @Override
    public void saveAll(AccountDTO[] accountDtos) {
        for (AccountDTO accountDTO: accountDtos) {
            log.info(accountDTO.toString());
        }
    }

    @Override
    public void saveAllList(List<AccountDTO> accountDTOList) {
        for (AccountDTO accountDTO : accountDTOList) {
            log.info(accountDTO.toString());
        }
    }

    @Override
    public void testParamsType(Object arg1, Object arg2, Object arg3) {
        log.info("arg1:{},{}",arg1.getClass(),arg1);
        log.info("arg2:{},{}",arg2.getClass(),arg2);
        log.info("arg3:{}",arg3);
    }
}
