package com.bizmda.bizsip.sample.source;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication
@ComponentScan(basePackages={"cn.hutool.extra.spring","com.bizmda.bizsip.sample"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class SampleSourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleSourceApplication.class, args);
    }
}
