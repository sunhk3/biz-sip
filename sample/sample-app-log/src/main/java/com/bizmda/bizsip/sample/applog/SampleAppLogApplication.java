package com.bizmda.bizsip.sample.applog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 史正烨
 */
@SpringBootApplication
public class SampleAppLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleAppLogApplication.class, args);
    }

}