package com.bizmda.bizsip.app.checkrule;

import lombok.Builder;
import lombok.Data;

/**
 * @author shizhengye
 */
@Data
@Builder
public class ServiceChcekRuleResult {
    private Object result;
}
