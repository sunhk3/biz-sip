package com.bizmda.bizsip.app.controlrule;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author shizhengye
 */
@Data
public class ControlRuleConfig {
    private String preRatingScript;
    private String ratingScript;
    private String updatingScript;
    private List<ControlRule> controlRuleList;
    private List<String> subControlRules;

    public ControlRuleConfig(Map<String,Object> controlRuleConfigmap) {
        this.preRatingScript = (String)controlRuleConfigmap.get("pre-rating-script");
        this.ratingScript = (String)controlRuleConfigmap.get("rating-script");
        this.updatingScript = (String)controlRuleConfigmap.get("updating-script");
        this.subControlRules = (List<String>)controlRuleConfigmap.get("sub-control-rules");
        if (this.subControlRules == null) {
            this.subControlRules = new ArrayList<>();
        }
        List<Map<String,Object>> mapList = (List<Map<String,Object>>)controlRuleConfigmap.get("rules");
        if (mapList == null) {
            mapList = new ArrayList<>();
        }
        this.controlRuleList = new ArrayList<>();
        for(Map<String,Object> map:mapList) {
            ControlRule controlRule = new ControlRule(map);
            this.controlRuleList.add(controlRule);
        }
    }
}
