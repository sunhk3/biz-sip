package com.bizmda.bizsip.app.executor;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.service.AppClientService;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @author shizhengye
 */
@Slf4j
public class SinkServiceAppExecutor extends AbstractAppExecutor {
    private final String sinkId;
    private AppClientService appClientService = null;

    public SinkServiceAppExecutor(String serviceId, String type, Map<String,Object> configMap,boolean isAppYml) {
        super(serviceId, type, configMap);
        this.sinkId = (String)(isAppYml?configMap.get("sink-id"):configMap.get("sinkId"));
        log.info("初始化sink-service类App服务[{}]: {}",serviceId,this.sinkId);
    }

    @Override
    public void init() {
        // 没有初始化内容
    }

    @Override
    public BizMessage<JSONObject> doAppService(BizMessage<JSONObject> message) throws BizException {
        log.debug("调用sink-service类App服务:\n{}",BizUtils.buildBizMessageLog(message));
        if (this.appClientService == null) {
            this.appClientService = SpringUtil.getBean(AppClientService.class);
        }
        BizMessage<JSONObject> result = this.appClientService.callSink(this.sinkId,message.getData());
        log.debug("sink-service类App服务返回:\n{}",BizUtils.buildBizMessageLog(result));
        return result;
    }
}
