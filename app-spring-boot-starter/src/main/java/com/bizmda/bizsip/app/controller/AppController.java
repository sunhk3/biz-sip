package com.bizmda.bizsip.app.controller;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.service.AppService;
import com.bizmda.bizsip.common.BizConstant;
import com.bizmda.bizsip.common.BizMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 史正烨
 */
@Slf4j
@RestController
public class AppController {
    @Autowired
    private AppService appService;

    @PostMapping(value="/api",consumes = "application/json", produces = "application/json")
    public BizMessage<JSONObject> doApiService(HttpServletRequest request,
                                               @RequestBody JSONObject inJsonObject) {
        String serviceId = request.getHeader(BizConstant.APP_SERVICE_SERVICE_ID);
        String traceId = request.getHeader(BizConstant.APP_SERVICE_TRACE_ID);
        return appService.process(serviceId,traceId,inJsonObject);
    }
}
