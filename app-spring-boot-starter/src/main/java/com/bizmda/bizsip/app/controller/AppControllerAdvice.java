package com.bizmda.bizsip.app.controller;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizTools;
import com.bizmda.log.trace.MDCTraceUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * @author 史正烨
 */
@RestControllerAdvice
@ResponseBody
public class AppControllerAdvice {
    @ExceptionHandler({ BizException.class })
    public BizMessage<JSONObject> bizException(BizException exception) {
        BizMessage<JSONObject> bizMessage = BizTools.bizMessageThreadLocal.get();
        BizMessage<JSONObject> outMessage = BizMessage.buildFailMessage(bizMessage,exception);
        BizTools.bizMessageThreadLocal.remove();
        MDCTraceUtils.removeTraceId();
        return outMessage;
    }
}
