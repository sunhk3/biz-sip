package com.bizmda.bizsip.app.executor;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import lombok.Data;

import java.util.Map;

/**
 * @author 史正烨
 */
@Data
public abstract class AbstractAppExecutor {
    protected String serviceId;
    protected String type;
    protected Map<String,Object> configMap;
    protected AbstractAppExecutor() {
    }

    protected AbstractAppExecutor(String serviceId, String type, Map<String,Object> configMap) {
        this.serviceId = serviceId;
        this.type = type;
        this.configMap = configMap;
    }

    /**
     * 服务整合器初始化
     */
    public abstract void init();

    /**
     * 调用聚合服务
     * @param message 传入的消息
     * @return 返回的消息
     * @throws BizException
     */
    public abstract BizMessage<JSONObject> doAppService(BizMessage<JSONObject> message) throws BizException;
}
